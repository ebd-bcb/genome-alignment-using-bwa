# base image:
FROM ubuntu:22.04 as genome_alignment
ARG DEBIAN_FRONTEND=noninteractive

# install packages:
RUN apt-get update && apt-get install -y \
    gcc-9 \
    g++-9 \
    make \
    git \
    wget \
    zlib1g-dev \
    build-essential autoconf automake libtool \
    libncurses5-dev libbz2-dev liblzma-dev \
    openjdk-11-jdk \
    ca-certificates	

RUN update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 100 && \
  update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-9 100

# install dependencies:

## intall bwa (https://github.com/lh3/bwa/releases/download/v0.7.17/bwa-0.7.17.tar.bz2):
WORKDIR /dependencies
COPY pkg/bwa-0.7.17.tar.bz2 .
RUN tar xf bwa-0.7.17.tar.bz2 && \
 cd bwa-0.7.17 && \
 make && \
 mv bwa /usr/local/bin/bwa

## install samtools (https://github.com/samtools/samtools/releases/download/1.9/samtools-1.9.tar.bz2):
WORKDIR /dependencies
COPY pkg/samtools-1.9.tar.bz2 .
RUN tar -xvf samtools-1.9.tar.bz2 && \
    rm samtools-1.9.tar.bz2 && \
    cd samtools-1.9 && \
    autoheader && \
    autoconf -Wno-syntax && \
    ./configure && \
    make && \
    make install
    
## install picard (https://github.com/broadinstitute/picard/releases/download/2.25.5/picard.jar):
WORKDIR /dependencies

COPY pkg/picard.jar /usr/local/bin/picard.jar
RUN ln -s /usr/local/bin/picard.jar /usr/local/bin/picard
ENV PATH="/usr/local/bin:${PATH}"


## install gatk:

WORKDIR /dependencies

### install conda:
COPY pkg/Miniconda3-4.3.11-Linux-x86_64.sh .
RUN chmod +x Miniconda3-4.3.11-Linux-x86_64.sh && \
    ./Miniconda3-4.3.11-Linux-x86_64.sh -b -p /opt/conda
ENV PATH=/opt/conda/bin:$PATH

### install GATK 3.7:
ENV CONDA_SSL_VERIFY=false
RUN conda install -c bioconda gatk=3.7

### install GenomeAnalysisTK (https://storage.googleapis.com/gatk-software/package-archive/gatk/GenomeAnalysisTK-3.7-0-gcfedb67.tar.bz2):
COPY pkg/GenomeAnalysisTK-3.7-0-gcfedb67.tar.bz2 .
RUN gatk-register GenomeAnalysisTK-3.7-0-gcfedb67.tar.bz2
 
# clean packages:
RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /
RUN rm -R /dependencies
